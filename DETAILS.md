# REST API Development
### Things todo list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/rest-api-development.git`
2. Go inside the folder: `cd rest-api-development`
3. Run `cp .env.example .env` then put your desired database name
4. Run `php artisan migrate`
5. Run `php artisan tinker`
6. Run `factory(\App\Product::class,50)->create()`    
7. Run `factory(\App\Review::class,50)->create()`        
8. Run `exit`
9. Run `php artisan route:list` to see available route list
    ```shell script
    +--------+-----------+-----------------------------------------+------------------+------------------------------------------------+--------------+
    | Domain | Method    | URI                                     | Name             | Action                                         | Middleware   |
    +--------+-----------+-----------------------------------------+------------------+------------------------------------------------+--------------+
    |        | GET|HEAD  | /                                       |                  | Closure                                        | web          |
    |        | GET|HEAD  | api/products                            | products.index   | App\Http\Controllers\ProductController@index   | api          |
    |        | POST      | api/products                            | products.store   | App\Http\Controllers\ProductController@store   | api          |
    |        | GET|HEAD  | api/products/{product}                  | products.show    | App\Http\Controllers\ProductController@show    | api          |
    |        | PUT|PATCH | api/products/{product}                  | products.update  | App\Http\Controllers\ProductController@update  | api          |
    |        | DELETE    | api/products/{product}                  | products.destroy | App\Http\Controllers\ProductController@destroy | api          |
    |        | GET|HEAD  | api/products/{product}/reviews          | reviews.index    | App\Http\Controllers\ReviewController@index    | api          |
    |        | POST      | api/products/{product}/reviews          | reviews.store    | App\Http\Controllers\ReviewController@store    | api          |
    |        | GET|HEAD  | api/products/{product}/reviews/{review} | reviews.show     | App\Http\Controllers\ReviewController@show     | api          |
    |        | PUT|PATCH | api/products/{product}/reviews/{review} | reviews.update   | App\Http\Controllers\ReviewController@update   | api          |
    |        | DELETE    | api/products/{product}/reviews/{review} | reviews.destroy  | App\Http\Controllers\ReviewController@destroy  | api          |
    |        | GET|HEAD  | api/user                                |                  | Closure                                        | api,auth:api |
    +--------+-----------+-----------------------------------------+------------------+------------------------------------------------+--------------+

    ```
10. Run `php artisan serve`


### List API

http://127.0.0.1:8000/api/products

http://127.0.0.1:8000/api/products/4

http://127.0.0.1:8000/api/products/4/reviews
